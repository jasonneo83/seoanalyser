﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeoAnalyser
{
    public partial class SEOAnalyser : System.Web.UI.Page
    {

        // Set your stop words here
        private string[] stopWords = new string[] { "or", "and", "a", "the", "is" };

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string rawInput = "";
                lblError.Text = "";

                // Determine whether source is URL or text?
                if (isURL(tbxInput.Text))
                {
                    // Append string and change all to lower case for consistency
                    rawInput = getUrlHtml(tbxInput.Text);
                }
                else
                {
                    // Append string and change all to lower case for consistency
                    rawInput = tbxInput.Text.ToLower();
                }


                // Contains any text or URL?
                if (rawInput.Length > 0)
                {

                    List<SEOReportObj> filteredReport = new List<SEOReportObj>();

                    // Decided to separate into array first as it takes more resource to loop a long sentences against stop words
                    // Also it is easier to filter stopwords from each word than sentence

                    // 1 Strip HTML, <script> and unicode off
                    if (chxNormalText.Checked)
                    {

                        StringBuilder txtInput = new StringBuilder();
                        txtInput.Append(Regex.Replace(rawInput, @"<script[^>]*>[\s\S]*?</script>|<[^>]*>|/(&.+;)/ig", String.Empty));

                        // Split texts/URL and add into list using StringBuffer
                        string[] rawInputs = txtInput.ToString().Split(new string[] { " ", Environment.NewLine, "\r", "\n", "\r\n", ". " }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string singleInput in rawInputs)
                        {
                            filteredReport.Add(new SEOReportObj(singleInput, 1));
                        }
                    }

                    // 2. Get meta tag words and add to existing list
                    if (chxMetaCount.Checked)
                    {
                        StringBuilder metaText = filterMetaContent(rawInput);

                        if (metaText.ToString().Length > 0)
                        {
                            string[] metaInputs = metaText.ToString().Split(new string[] { " ", Environment.NewLine, ",", ". " }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (string singleInput in metaInputs)
                            {
                                filteredReport.Add(new SEOReportObj(singleInput, 1));
                            }
                        }

                    }

                    // 3. Filter Stop Words
                    if (chxStopWords.Checked && filteredReport.Count > 0)
                    {
                        filteredReport = filteredReport.Where(x => !stopWords.Contains(x.word)).ToList();
                    }

                    // 4. Group and count duplicate words
                    if (chxWordsCount.Checked && filteredReport.Count > 0)
                    {
                        filteredReport = filteredReport.GroupBy(x => x.word).Select(n => new SEOReportObj(n.Key, n.Count())).ToList();
                    }

                    // 5. Count number of URLs
                    if (!chxUrlCount.Checked && filteredReport.Count > 0)
                    {
                        // Already added by default. Just remove if untick
                        filteredReport.RemoveAll(x => x.word.StartsWith("http://") || x.word.StartsWith("https://") || x.word.StartsWith("www."));
                    }

                    // 6. Generate Report
                    if (filteredReport != null)
                    {
                        // Store final report for ordering purpose
                        ViewState["result"] = filteredReport;

                        // Display to gridview
                        gridViewResult.DataSource = filteredReport;
                        gridViewResult.DataBind();

                    }
                    else
                    {
                        lblError.Text = "No report found";
                    }
                }
            }
            catch (Exception exp)
            {
                lblError.Text = "Submit Click: " + exp.Message;
            }
        }

        private StringBuilder filterMetaContent(string rawInput)
        {

            StringBuilder metaText = null;

            try
            {
                string regexMetaContent = "<meta[\\s]+[^>]*?name[\\s]?=[\\s\"\']+(.*?)[\\s\"\']+content[\\s]?=[\\s\"\']+(.*?)[\"\']+.*?>";
                Regex regex = new Regex(regexMetaContent, RegexOptions.IgnoreCase);
                metaText = new StringBuilder();

                // Cater for more than one matches
                foreach (Match itemMatch in regex.Matches(rawInput))
                {
                    for (int i = 1; i < itemMatch.Groups.Count; i++)
                    {
                        metaText.Append(itemMatch.Groups[i].Value).Append(" ");
                    }
                }
            }
            catch (Exception exp)
            {
                lblError.Text = "getMetaContent: " + exp.Message;
            }

            return metaText;

        }

        private string getUrlHtml(string url)
        {

            string htmlCode = "";

            try
            {
                IWebProxy defaultWebProxy = WebRequest.DefaultWebProxy;
                defaultWebProxy.Credentials = CredentialCache.DefaultCredentials;

                using (WebClient client = new WebClient() { Proxy = defaultWebProxy })
                {
                    htmlCode = client.DownloadString(url);
                }

            }
            catch (Exception exp)
            {
                lblError.Text = "getUrlHtml: " + exp.Message;
            }

            return htmlCode;

        }

        private bool isURL(string uriName)
        {
            Uri uriResult;
            bool result = Uri.TryCreate(uriName, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            return result;
        }

        protected void gridViewResult_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                // Retrieve filtered results from view state
                List<SEOReportObj> filteredReport = (List<SEOReportObj>)ViewState["result"];

                if (filteredReport != null && filteredReport.Count > 0)
                {

                    string sorting = getColumnOrder(e.SortExpression);

                    if (sorting == "ASC")
                    {
                        if (e.SortExpression == "word")
                            filteredReport = filteredReport.OrderBy(x => x.word).ToList();
                        else
                            filteredReport = filteredReport.OrderBy(x => x.count).ToList();
                    }
                    else
                    {
                        if (e.SortExpression == "word")
                            filteredReport = filteredReport.OrderByDescending(x => x.word).ToList();
                        else
                            filteredReport = filteredReport.OrderByDescending(x => x.count).ToList();
                    }

                    // Display to gridview
                    gridViewResult.DataSource = filteredReport;
                    gridViewResult.DataBind();

                }

            }
            catch (Exception exp)
            {
                lblError.Text = "Result Sort: " + exp.Message;
            }
        }

        private string getColumnOrder(string columnName)
        {

            // Manual ordering due to GridView bug
            if (ViewState[columnName] == null)
            {
                ViewState[columnName] = "ASC";
            }
            else
            {
                if ((String)ViewState[columnName] == "ASC")
                {
                    ViewState[columnName] = "DESC";
                }
                else
                {
                    ViewState[columnName] = "ASC";
                }
            }

            return (string)ViewState[columnName];

        }

        [Serializable]
        public class SEOReportObj
        {
            public string word { get; set; }
            public int count { get; set; }

            public SEOReportObj(string word, int count)
            {
                this.word = word;
                this.count = count;
            }

        }

    }
}