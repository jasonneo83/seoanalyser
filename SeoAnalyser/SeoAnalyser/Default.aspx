﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SeoAnalyser.SEOAnalyser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <div class="row">
            <div class="column">
                <label>Enter URL or paragraphs:</label>
                <br /><br />
                <asp:TextBox runat="server" ID="tbxInput" TextMode="MultiLine" Width="90%" Height="400px"></asp:TextBox>
                <br /><br />
                <asp:CheckBox runat="server" Checked="true" ID="chxNormalText" /> Include normal text
                <br /><br />
                <asp:CheckBox runat="server" Checked="true" ID="chxMetaCount" /> Include Meta Words
                <br /><br />
                <asp:CheckBox runat="server" Checked="true" ID="chxStopWords"/> Filter Stop Words
                <br /><br />
                <asp:CheckBox runat="server" Checked="true" ID="chxWordsCount" /> Group Words and count
                <br /><br />
                <asp:CheckBox runat="server" Checked="true" ID="chxUrlCount" /> Include URL links (Not in HTML)
                <br /><br />
                <asp:Label runat="server" style="color: red;" ID="lblError"></asp:Label>
                <br /><br />
                <asp:Button runat="server" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Submit" />
            </div>
            <div class="column">
                <asp:GridView runat="server" ID="gridViewResult" AllowSorting="true" OnSorting="gridViewResult_Sorting"></asp:GridView>
            </div>
        </div> 

    </form>
</body>
</html>

<style>
    .row {
      display: flex;
    }

    .column {
      flex: 50%;
    }
</style>